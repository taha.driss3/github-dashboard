import React, { useEffect } from 'react';
import { connect } from "react-redux"
import { findUserByName, findRepoByName } from "../redux/github/github.action"
import ProfileCard from "./profileCard"
import ListRepo from "./listRepo"
const Dashboard = (props) => {
    useEffect(() => {
        console.log("test")
        const { userName } = props.match.params;
        props.findUserByName(userName);
        props.findRepoByName(userName);

    })
    useEffect(() => {
        console.log(props.resFindByName)
        const { status, data } = props.resFindByName;
        if (status === 200) {
            console.log(data)
        }
    }, [props.resFindByName])

    return (

        <div>
            <ProfileCard ></ProfileCard>
            <ListRepo></ListRepo>
        </div >

    );
}

const mapStateToProps = state => ({
    resFindByName: state.github.resFindByName
})


export default connect(mapStateToProps, { findUserByName, findRepoByName })(Dashboard);