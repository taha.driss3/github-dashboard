import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import { connect } from "react-redux"
import { Redirect } from "react-router-dom";


const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    inline: {
        display: 'inline',
    },
}));

const AlignItemsList = (props) => {
    const [redirect, setRedirect] = useState(false);
    const [userSelected, setUserSelected] = useState("");

    const classes = useStyles();
    const { userList } = props;

    return (
        <List className={classes.root} component="nav">
            {redirect ? <Redirect to={`dashboard/${userSelected}`} /> : null}
            {userList.map(item => {

                return (
                    <ListItem key={item.login} dense button onClick={() => { setUserSelected(item.login); setRedirect(true); }} >
                        <ListItemAvatar>
                            <Avatar alt={item.login} src={item.avatar_url} />
                        </ListItemAvatar>
                        <ListItemText primary={item.login} />

                    </ListItem>
                );
            })}

        </List>
    );
}
const mapStateToProps = state => ({ userList: state.github.resSearchByName.status === 200 ? state.github.resSearchByName.data.items : [] })
export default connect(mapStateToProps, {})(AlignItemsList);
