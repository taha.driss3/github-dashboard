
import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import InputBase from "@material-ui/core/InputBase";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import SearchIcon from "@material-ui/icons/Search";
import { connect } from "react-redux";
import { searchByUserName } from "../redux/github/github.action";
import AlignItemsList from './listAvatar.js'

const useStyles = makeStyles({
    root: {
        padding: "2px 4px",
        display: "flex",
        alignItems: "center",
        width: "100%",
        marginTop: 15,
        marginBottom: 15
    },
    input: {
        marginLeft: 8,
        flex: 1
    },
    iconButton: {
        padding: 10
    },
    divider: {
        width: 1,
        height: 28,
        margin: 4
    }
});
const Search = (props) => {
    const [query, setQuery] = useState("");
    const [listDisplay, setListDisplay] = useState(false);

    useEffect(() => {
        const { resSearch } = props;
        if (resSearch.status === 200) {
            console.log("ok");
            setListDisplay(true);
            console.log(resSearch.data);
        } else console.log("not ok")
    }, [props.resSearch]);

    const classes = useStyles();
    const handleChange = (event) => {
        const queryTrimmed = event.target.value.trim();
        setQuery(queryTrimmed)
    };
    return (
        <div> <Paper className={classes.root}>
            <IconButton className={classes.iconButton} aria-label="Menu">
                <MenuIcon />
            </IconButton>
            <InputBase
                className={classes.input}
                placeholder="Search "
                inputProps={{ "aria-label": "Search" }}
                name="query"
                onKeyUp={handleChange}
            />
            <IconButton className={classes.iconButton} aria-label="Search" onClick={() => query.trim().length !== 0 ? props.searchByUserName(query) : null}>
                <SearchIcon />
            </IconButton>
        </Paper>
            {listDisplay ? <AlignItemsList /> : null}
        </div>

    );
}
const mapStateToProps = state => ({ resSearch: state.github.resSearchByName })
export default connect(mapStateToProps, { searchByUserName })(Search);