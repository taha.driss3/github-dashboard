import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';

import { connect } from "react-redux"
import Plus from '@material-ui/icons/Add';
import { Redirect } from "react-router-dom";


const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    inline: {
        display: 'inline',
    },
}));

const AlignItemsList = (props) => {
    const [redirect, setRedirect] = useState(false);
    const [repoSelected, setRepoSelected] = useState("");

    const classes = useStyles();
    const { repoList, resFindByName } = props;

    return (
        <List className={classes.root} component="nav">
            {redirect ? <Redirect to={`/repo/${resFindByName.login}/${repoSelected}`} /> : null}
            {repoList.map(item => {

                return (
                    <ListItem key={item.name} dense button onClick={() => { setRepoSelected(item.name); setRedirect(true); }} >
                        <ListItemIcon>
                            <Plus />
                        </ListItemIcon>
                        <ListItemText primary={item.name} />

                    </ListItem>
                );
            })}

        </List>
    );
}
const mapStateToProps = state => ({ repoList: state.github.resFindRepo ? state.github.resFindRepo : [], resFindByName: state.github.resFindByName.data })
export default connect(mapStateToProps, {})(AlignItemsList);
