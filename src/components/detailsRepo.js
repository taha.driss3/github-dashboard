import React, { useEffect } from 'react';
import { connect } from "react-redux"
import { findDetailRepo } from "../redux/github/github.action"
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import moment from "moment";
import Chip from '@material-ui/core/Chip';
import Star from '@material-ui/icons/Star';




const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',

    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
        width: 151,
        height: 151
    },
    controls: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: theme.spacing(1),
        paddingBottom: theme.spacing(1),
    },

}));
const DetailsRepos = (props) => {
    const classes = useStyles();
    useEffect(() => {
        const { userName, repoName } = props.match.params
        props.findDetailRepo(userName, repoName)
    })



    const getDateFormated = (stringDate) => {

        let dateObj = new Date(stringDate);
        let momentObj = moment(dateObj);
        let momentString = momentObj.format('YYYY-MM-DD'); // 2016-07-15

        return momentString;

    }


    return (
        props.repo.owner ? <><Card className={classes.root} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <CardMedia
                className={classes.cover}
                image={props.repo.owner.avatar_url}
                title="Live from space album cover"
            />
            <div className={classes.details}>
                <CardContent className={classes.content}>
                    <Typography component="h5" variant="h5">
                        <a href={props.repo.owner.html_url} target="_blank"> <Button>See the github of {props.repo.owner.login} now</Button></a>
                    </Typography>

                </CardContent>

            </div>

        </Card>
            <Card className={classes.root} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>

                <div className={classes.details}>
                    <CardContent className={classes.content}>
                        <Typography variant="h5" component="h2">
                            {props.repo.name}
                        </Typography>
                        <Typography color="textSecondary">
                            {props.repo.language}
                        </Typography>

                        <Typography paragraph>
                            {props.repo.description}
                        </Typography>
                        <Typography variant="body2" component={'span'}>
                            <Chip
                                icon={<Star />}
                                label={props.repo.stargazers_count}
                                variant="outlined"
                            />  <br />

                            Created :  {getDateFormated(props.repo.created_at)} <br />
                            Last Update : {getDateFormated(props.repo.updated_at)}
                        </Typography>
                    </CardContent>

                </div>

            </Card> </> : "loading"
    );

}

const mapStateToProps = (state) => ({ repo: state.github.resDetailRepo })
export default connect(mapStateToProps, { findDetailRepo })(DetailsRepos);