import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import rootReducer from "./root-reducer";
import { composeWithDevTools } from 'redux-devtools-extension'


const initialState = {};
const middlewares = [thunk];

const middlewareEnhancer = applyMiddleware(...middlewares)

//const enhancers = [middlewareEnhancer, monitorReducersEnhancer]

const store = createStore(
  rootReducer,
  initialState,
  composeWithDevTools(
    middlewareEnhancer

  )
);

export default store;
