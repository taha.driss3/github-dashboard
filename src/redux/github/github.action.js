import {
  GITHUB
} from "../types";
import { API_GITHUB } from "../../utils/api";
import axios from "axios";

export const searchByUserName = (userName) => async dispatch => {
  try {
    const res = await axios.get(`${API_GITHUB}/search/users?q=${userName}`);

    dispatch({
      type: GITHUB.SEARCH_BY_NAME,
      payload: res
    });
  } catch (err) {
    if (err.response.status !== 200) {
      alert("Service temporairement indisponible")
    }
    return dispatch({
      type: GITHUB.SEARCH_BY_NAME,
      payload: err.response
    });
  }
};
export const findUserByName = (userName) => async dispatch => {
  try {
    const res = await axios.get(`${API_GITHUB}/users/${userName}`);

    dispatch({
      type: GITHUB.FIND_USER_BY_NAME,
      payload: res
    });
  } catch (err) {
    if (err.response.status !== 200) {
      alert("Service temporairement indisponible")
    }
    return dispatch({
      type: GITHUB.FIND_USER_BY_NAME,
      payload: err.response
    });
  }
};
export const findRepoByName = (userName) => async dispatch => {
  try {
    const res = await axios.get(`${API_GITHUB}/users/${userName}/repos`);
    dispatch({
      type: GITHUB.FIND_REPO_BY_NAME,
      payload: res
    });
  } catch (err) {
    if (err.response.status !== 200) {
      alert("Service temporairement indisponible")
    }
    return dispatch({
      type: GITHUB.FIND_REPO_BY_NAME,
      payload: err.response
    });
  }
};
export const findDetailRepo = (userName, repoName) => async dispatch => {
  try {
    const res = await axios.get(`${API_GITHUB}/repos/${userName}/${repoName}`);
    dispatch({
      type: GITHUB.FIND_DETAIL_REPO,
      payload: res.data
    });
  } catch (err) {
    console.log(err);

    return dispatch({
      type: GITHUB.FIND_DETAIL_REPO,
      payload: err.response
    });
  }
};
