import {
  GITHUB
} from "../types";

const initialState = {
  resSearchByName: {},
  resFindByName: {},
  resFindRepo: [],
  resDetailRepo: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GITHUB.SEARCH_BY_NAME:
      return {
        ...state,
        resSearchByName: action.payload
      };
    case GITHUB.FIND_USER_BY_NAME:
      return {
        ...state,
        resFindByName: action.payload
      };
    case GITHUB.FIND_REPO_BY_NAME:
      return {
        ...state,
        resFindRepo: action.payload.data
      };
    case GITHUB.FIND_DETAIL_REPO:
      return {
        ...state,
        resDetailRepo: action.payload
      };

    default:
      return state;
  }
}
