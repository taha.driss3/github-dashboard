/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import './App.css';
import Header from './components/layouts/header'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Paper from "@material-ui/core/Paper";
import Search from './components/search';
import { makeStyles } from "@material-ui/core/styles";
import { Provider } from "react-redux";
import store from "./redux/store.js";
import Dashboard from './components/dashboard';
import DetailsRepo from './components/detailsRepo';




const useStyles = makeStyles(theme => ({
  root: {
    padding: 100,
  },

}));

function App() {
  const classes = useStyles();

  return (
    <div className="App">
      <Provider store={store}>

        <Router>
          <Header />
          <div className={classes.root}>
            <Paper >
              <Switch>
                <Route exact path="/" render={props => <Search {...props} />} />
                <Route path="/dashboard/:userName" component={Dashboard} />
                <Route path="/repo/:userName/:repoName" component={DetailsRepo} />

              </Switch>
            </Paper>
          </div>
        </Router>

      </Provider>

    </div>
  );
}

export default App;
